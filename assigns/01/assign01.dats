(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2018
//
// Classroom: CAS 220
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign01: 20 points
//
// Out date: Friday, May 25th, 2018
// Due date: Tuesday, May 29th, 2018 // by midnight
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "ASSIGN01"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
extern
fun
fact(n: int): int
//
implement
fact(n) =
if n > 0 then n * fact(n-1) else 1
//
(* ****** ****** *)
//
// HX: 5 points
// Please implement a function [try_fact] that
// finds the first [n] such that fact(n) equals 0:
//
extern
fun
try_fact((*void*)): int
//
(* ****** ****** *)
//
// HX: 5 points
// Please implement a function [sumup_3_5_fact] that
// sums up all the natural numbers less than [n0] that
// is a multiple of 3 or 5 but not both.
//
extern
fun
sumup_3_5(n0: int): int
//
(* ****** ****** *)
//
// HX: 10 points
(*
Given a natural number n, ##fun(intsqrt) returns the integer k
satisfying k*k <= n < (k+1)*(k+1). Please give an implementation of
##fun(intsqrt) that is of the time-complexity O(log(n)). The interface
for ##fun(intsqrt) is given as follows:
*)
//
extern
fun
intsqrt(n0: int): int // it return the largest integer k satisfying k*k <= n0
//
(* ****** ****** *)


(* end of [assign01.dats] *)
