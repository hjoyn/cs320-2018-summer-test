#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

datatype
intlist = 
| nil of ()
| cons of (int, intlist)

(* ****** ****** *)

val xs0 = nil()
val xs1 = cons(1, nil())
val xs2 = cons(1, cons(2, nil()))

(* ****** ****** *)
//
// HX-2018-05-29: not tail-recursive!
//
fun
fromto
(start: int, finish: int): intlist =
(
  if
  (start < finish)
  then cons(start, fromto(start+1, finish))
  else nil()
)
//
(* ****** ****** *)

val xs10 = fromto(1, 11)

(* ****** ****** *)

fun
length(xs: intlist): int =
case xs of
| nil() => 0 // guarded by the pattern 'nil()'
| cons(x, xs2) => 1 + length(xs2) // guarded by the pattern 'cons(_, _)'

(* ****** ****** *)

fun
foreach
( xs: intlist
, fwork: (int) -<cloref1> void): void =
(
case xs of
| nil() => ()
| cons(x, xs2) => (fwork(x); foreach(xs2, fwork))
)

(* ****** ****** *)
//
// HX: not tail-recursive!
//
fun
append
( xs: intlist
, ys: intlist): intlist =
(
case xs of
| nil() => ys
| cons(x, xs) => cons(x, append(xs, ys))
)
//
(* ****** ****** *)

fun sing(x: int): intlist = cons(x, nil())

fun
extend
(xs: intlist, x: int): intlist = append(xs, sing(x))

(* ****** ****** *)
//
// HX:
// Correct
// but EXTREMELY inefficient!!!
// It is O(n^2)-time
//
fun
reverse
(xs: intlist): intlist =
(
case xs of
| nil() => nil()
| cons(x, xs) => extend(reverse(xs), x)
)
//
(* ****** ****** *)

fun
reverse
(
xs: intlist
) : intlist = loop(xs, nil()) where
{
  fun
  loop
  (xs: intlist, ys: intlist): intlist =
    case xs of
    | nil() => ys
    | cons(x, xs) => loop(xs, cons(x, ys))
}

(* ****** ****** *)


fun
map
( xs: intlist
, fopr: int -<cloref1> int): intlist =
(
case xs of
| nil() => nil()
| cons(x, xs) => cons(fopr(x), map(xs, fopr))
)

(* ****** ****** *)

val () = foreach(xs2, lam(x) => println!(x))
val () = println!("######")
val () = foreach(append(xs2, xs2), lam(x) => println!(x))
val () = println!("######")
val () = foreach(reverse(append(xs2, xs2)), lam(x) => println!(x))
val () = println!("######")
val () = foreach(reverse(reverse(append(xs2, xs2))), lam(x) => println!(x))

(* ****** ****** *)

val ys = map(fromto(0, 10), lam(x) => ~x)
val () = println!("######")
val () = foreach(ys,  lam(x) => println!(x))
val zs = map(ys, lam(x) => abs(x))
val () = foreach(zs,  lam(x) => println!(x))

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [intlist.dats] *)
